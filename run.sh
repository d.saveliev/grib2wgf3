#!/bin/bash

site=http://openskiron.org/en/openskiron

# delete old files
find . -type f -name "*grb" -exec rm -rf {} \;

# delete all folders with data
find . -type d -regex ".*\([0-9]+\)+_[0-9]+" -exec rm -rf {} \;

# download
curl -s $site | grep -Po '(?<=href=")http://openskiron.org/gribs/[^"]*' | xargs wget

# unzip
find . -type f -name "*.bz2" -exec bunzip2 {} +

# convert
find -name \*.grb | xargs ./grib2wgf3