#!/bin/bash

#$a = "$(find . -type f \'\(.*cpp\|.*hpp\)\' -print0)"

cppFiles="$(find libs/ -type f -regex '.*cpp\|.*hpp' -print0 | xargs -0 echo) "

g++ -std=c++11 grib2wgf3.cpp -o grib2wgf3 $cppFiles -Ilibs /usr/lib/libgrib_api.a -lpthread \
	-ljasper -lpng -laec