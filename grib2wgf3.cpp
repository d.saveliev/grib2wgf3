
#include <strings.h>
#include <stdio.h>
#include <iostream>
#include <sys/stat.h>
#include <grib_api.h>
#include <unistd.h>

#include "libs/wgf3.h"

void usage(char* prog)
{
	printf("Usage: grib2wgf3 <input grib-1 file> [<input grib-1 file> <input grib-1 file> ...]\n");
	exit(1);
}

tm* toTimestamp(long date, long timeshift = 0, long starttime = 0)
{
	struct tm* tmtime = new tm();

	tmtime->tm_mday = date % 100;
	date /= 100;
	tmtime->tm_mon = date % 100 - 1;
	tmtime->tm_year = date / 100 - 1900;
	tmtime->tm_hour = starttime + timeshift;
	tmtime->tm_min = 0;
	tmtime->tm_sec = 0;

	return tmtime;
}

char* getDirectoryName(char* dirName, tm* tmtime)
{
	//01-08-2017-01_12345678
	memset(dirName,0,64);

	int timestamp = timegm(tmtime);
	strftime(dirName, 24, "%d-%m-%Y-%H", tmtime);
	sprintf(dirName, "%s_%d/", dirName, timestamp);
	return dirName;
}

void processFile(char* filename)
{
	FILE* in = NULL;
	WGF3Header* wgfHeader = new WGF3Header;
	WGF3* wgf = new WGF3();
	WGF3* wgfOld = new WGF3();
	struct tm* forcastTime;
	struct stat st = { 0 };
	char wgfFileName[10] = { 0, };
	char directoryName[64] = { 0, };
	char shortName[32] = { 0, };
	double lat, lon, value, missingValue = 0;
	size_t len;
	int err = 0;
	grib_handle *h = NULL;
	grib_iterator* iter = NULL;
	long date, time, startStep, latMin, latMax, lonMin, lonMax, Ni, Nj = 0;
	bool needConcat = false;

	in = fopen(filename, "r");
	if (!in) {
		fprintf(stderr, "ERROR: unable to open file %s\n", filename);
		perror("Error");
		return;
	}

	/* Loop on all the messages in a file.*/
	while ((h = grib_handle_new_from_file(0, in, &err)) != NULL) {
		/* Check of errors after reading a message. */
		if (err != GRIB_SUCCESS) GRIB_CHECK(err, 0);

		len = 32;
		GRIB_CHECK(grib_get_string(h, "shortName", shortName, &len), 0);

		if (strcmp(shortName, "10u") == 0) {
			strcpy(wgfFileName, "UGRD.wgf3");
		} else if (strcmp(shortName, "10v") == 0) {
			strcpy(wgfFileName, "VGRD.wgf3");
		} else {
			continue;
		}

		GRIB_CHECK(grib_get_long(h, "dataDate", &date), 0);
		GRIB_CHECK(grib_get_long(h, "dataTime", &time), 0);
		GRIB_CHECK(grib_get_long(h, "startStep", &startStep), 0);
		GRIB_CHECK(grib_get_long(h, "longitudeOfFirstGridPoint", &lonMin), 0);
		GRIB_CHECK(grib_get_long(h, "latitudeOfFirstGridPoint", &latMin), 0);
		GRIB_CHECK(grib_get_long(h, "longitudeOfLastGridPoint", &lonMax), 0);
		GRIB_CHECK(grib_get_long(h, "latitudeOfLastGridPoint", &latMax), 0);
		GRIB_CHECK(grib_get_long(h, "Ni", &Ni), 0);
		GRIB_CHECK(grib_get_long(h, "Nj", &Nj), 0);

		printf("##%s %s - %d %d %d\n", filename, shortName, date, time, startStep);

		forcastTime = toTimestamp(date, startStep);
		getDirectoryName(directoryName, forcastTime);

		if (stat(directoryName, &st) == -1) {
			mkdir(directoryName, 0755);
		}

		strcat(directoryName, wgfFileName);

		if( access( directoryName, F_OK ) != -1 ) { //wgf if file exists bounds needed to be expanded
			if (!wgfOld->open(directoryName)){
				fprintf(stderr, "ERROR: unable to open WGF3 file %s\n", directoryName);
				return;
			}
			*wgfHeader = wgfOld->getHeader();
			if (wgfHeader->latMax > latMax) latMax = wgfHeader->latMax; //expand bounds
			if (wgfHeader->lonMax > lonMax) lonMax = wgfHeader->lonMax;
			if (wgfHeader->latMin < latMin) latMin = wgfHeader->latMin;
			if (wgfHeader->lonMin < lonMin) lonMin = wgfHeader->lonMin;
			Nj = (latMax - latMin) / wgfHeader->dLat + 1; // assume that grid is the same as in the old file
			Ni = (lonMax - lonMin) / wgfHeader->dLat + 1;
			needConcat = true;
		} else {
			needConcat = false;
		}

		if (!wgf->open(directoryName,
						latMin,
						latMax,
						lonMin,
						lonMax,
						((latMax - latMin) / (Nj - 1)),
						((lonMax - lonMin) / (Ni - 1)),
						255,
						true))
		{
			fprintf(stderr, "ERROR: unable to open WGF3 file %s\n", directoryName);
			return;
		}

		if (needConcat) {
			for (int i = wgfHeader->latMin; i <= wgfHeader->latMax; i += wgfHeader->dLat){
				for (int j = wgfHeader->lonMin; j <= wgfHeader->lonMax; j += wgfHeader->dLon){
					wgf->write(i, j, wgfOld->read(i, j));//TODO: rewrite this to memcpy
				}
			}
			wgfOld->close();
		}

		GRIB_CHECK(grib_get_double(h, "missingValue", &missingValue), 0);
		iter = grib_iterator_new(h, 0, &err);
		if (err != GRIB_SUCCESS) GRIB_CHECK(err, 0);

		/* Loop on all the lat/lon/values. */
		while (grib_iterator_next(iter, &lat, &lon, &value)) {
			wgf->writeAvg(lat * 1000, lon * 1000, value);
		}

		wgf->close();

		grib_iterator_delete(iter);
		grib_handle_delete(h);
	}

	fclose(in);

	delete wgf;
	delete wgfHeader;
	delete forcastTime;
	delete wgfOld;
}


int main(int argc,char **argv)
{
	if (argc < 2) usage(argv[0]);
	for (int i = 1; i <= argc - 1; i++){
		processFile(argv[i]);
	}
	return 0;
}

